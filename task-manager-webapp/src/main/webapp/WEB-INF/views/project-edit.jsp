<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT EDIT</h1>
<form action="/project/edit/${project.id}/" method="post">
    <input type="hidden" name="id" value="${project.id}"/>
    <p>
    <div>NAME:</div>
    <div><input type="text" name="name" value="${project.name}"/></div>
    <p>
    <div>DESCRIPTION:</div>
    <div><input type="text" name="description" value="${project.description}"/></div>
    <button type="submit">SAVE PROJECT</button>
</form>
<jsp:include page="../include/_footer.jsp"/>
