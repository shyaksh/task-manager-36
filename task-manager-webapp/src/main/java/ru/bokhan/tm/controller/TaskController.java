package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.repository.TaskRepository;


@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/task/create")
    public String create() {
        @NotNull final Task task = new Task();
        taskRepository.save(task);
        return String.format("redirect:/task/edit/%s", task.getId());
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @Nullable final Task task = taskRepository.findById(id).orElse(null);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(Task task) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }
}
