package ru.bokhan.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bokhan.tm.entity.AbstractEntity;

public interface AbstractRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}
