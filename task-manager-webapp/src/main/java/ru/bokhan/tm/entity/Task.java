package ru.bokhan.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    public Task(
            @NotNull String id,
            @NotNull String name,
            @Nullable String description
    ) {
        super(id);
        this.name = name;
        this.description = description;
    }

}
