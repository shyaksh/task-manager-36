package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @WebMethod
    void closeSession(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void closeSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @Nullable
    @WebMethod
    UserDTO getUserBySession(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @NotNull
    @WebMethod
    String getUserIdBySession(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @NotNull
    @WebMethod
    List<SessionDTO> findSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @Nullable
    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    void signOutByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    void signOutByUserId(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void removeSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void removeSession(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

}
