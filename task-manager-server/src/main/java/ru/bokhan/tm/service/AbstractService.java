package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.AbstractEntityDTO;
import ru.bokhan.tm.entity.AbstractEntity;
import ru.bokhan.tm.exception.incorrect.IncorrectDataFileException;
import ru.bokhan.tm.repository.dto.AbstractRepositoryDTO;
import ru.bokhan.tm.repository.entity.AbstractRepository;

import javax.transaction.Transactional;
import java.util.List;

public abstract class AbstractService<D extends AbstractEntityDTO, E extends AbstractEntity> implements IService<D, E> {

    @Autowired
    private AbstractRepositoryDTO<D> dtoRepository;

    @Autowired
    private AbstractRepository<E> repository;

    @Override
    public @NotNull List<D> findAll() {
        @NotNull final List<D> result = dtoRepository.findAll();
        return result;
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void load(@Nullable final List<D> list) {
        if (list == null) throw new IncorrectDataFileException();
        repository.deleteAll();
        dtoRepository.saveAll(list);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        repository.delete(entity);
    }

    @Override
    public void remove(@Nullable final D entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

    @Override
    public void save(@NotNull D entity) {
        dtoRepository.save(entity);
    }

}