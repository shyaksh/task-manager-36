package ru.bokhan.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.api.service.IPropertyService;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.ProjectDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.endpoint.AbstractEndpoint;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.system.NotFoundException;

import javax.xml.ws.Endpoint;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    //@Autowired
    private void initData(
            @NotNull IUserService userService,
            @NotNull IProjectService projectService,
            @NotNull ITaskService taskService
    ) {
        userService.create("user", "user", "user@test.ru");
        @Nullable final UserDTO user = userService.findByLogin("user");
        if (user == null) throw new NotFoundException();
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        @Nullable final ProjectDTO userProject1 = projectService.findByName(user.getId(), "UserProject1");
        if (userProject1 == null) throw new NotFoundException();
        taskService.create(user.getId(), userProject1.getId(), "UserTask1");
        taskService.create(user.getId(), userProject1.getId(), "UserTask2");
        userService.create("admin", "admin", Role.ADMIN);
        @Nullable final UserDTO admin = userService.findByLogin("admin");
        if (admin == null) throw new NotFoundException();
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
        @Nullable final ProjectDTO adminProject2 = projectService.findByName(admin.getId(), "AdminProject2");
        if (adminProject2 == null) throw new NotFoundException();
        taskService.create(admin.getId(), adminProject2.getId(), "AdminTask1");
        taskService.create(admin.getId(), adminProject2.getId(), "AdminTask2");
    }

    private void initEndpoint() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull String name = endpoint.getClass().getSimpleName();
        @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(@Nullable final String[] arguments) {
        System.out.println("** TASK MANAGER SERVER **");
        initEndpoint();
    }

}